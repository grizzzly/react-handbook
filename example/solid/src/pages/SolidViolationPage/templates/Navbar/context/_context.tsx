import React, { createContext, Dispatch, useContext, useReducer } from "react";

export enum eDisconnectReason {
  ANOTHER_BROWSER_LOGIN = "another browser login",
  DISCONNECTED = "disconnected",
  NO_INTERNET_CONNECTION = "no internet connection",
}

export enum eConnectStatus {
  ONLINE = "online",
  OFFLINE = "offline",
}

export interface INavbarContextState {
  anotherBrowserLogin: boolean;
  isOnline: boolean;
  connectStatus: eConnectStatus;
}

export interface INavbarContextProps {
  state: INavbarContextState;
  dispatch: Dispatch<{ key: keyof INavbarContextState; payload: any }>;
}

export const NavbarContext = createContext({} as INavbarContextProps);

const reducer = (
  state: INavbarContextState,
  { key, payload }: { key: keyof INavbarContextState; payload: any }
) => {
  return {
    ...state,
    [key]: payload,
  };
};

export const NavbarContextProvider: React.FC<{ children: React.ReactNode }> = ({
  children,
}) => {
  const [state, dispatch] = useReducer(reducer, {
    anotherBrowserLogin: false,
    isOnline: true,
    connectStatus: eConnectStatus.ONLINE,
  });

  return (
    <NavbarContext.Provider value={{ state, dispatch }}>
      {children}
    </NavbarContext.Provider>
  );
};

export const useNavbarContext = () => useContext(NavbarContext);
