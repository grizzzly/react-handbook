import React from "react";
import { NavbarContextProvider } from "./context";
import { NavbarLinks } from "./fragments";
import { ToogleButton } from "./fragments/ToogleButton";
import { UserIconWrapper } from "./fragments/UserIconWrapper";

import styles from "./_styles.module.css";

export const Navbar: React.FC = () => {
  return (
    <NavbarContextProvider>
      <nav className={styles.navbar}>
        <NavbarLinks />
        <ToogleButton />
        <UserIconWrapper />
      </nav>
    </NavbarContextProvider>
  );
};
