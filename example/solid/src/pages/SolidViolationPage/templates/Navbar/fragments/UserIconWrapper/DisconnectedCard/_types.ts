import React from "react";
import { eDisconnectReason } from "../../../context";

export interface IDisconnectedCard {
  children: React.ReactNode;
  disconnectReason: eDisconnectReason;
}
