import { useMemo } from "react";
import { eDisconnectReason } from "../../../context";

export const useDisconnectReasonComponent = (reason: eDisconnectReason) => {
  const reasonComponent = useMemo(() => {
    const reasonText = {
      [eDisconnectReason.ANOTHER_BROWSER_LOGIN]: "Disconnected",
      [eDisconnectReason.DISCONNECTED]: "Reconnecting...",
      [eDisconnectReason.NO_INTERNET_CONNECTION]: "Offline",
    };

    if (reason !== null) return <p>{reasonText[reason]}</p>;

    return null;
  }, [reason]);

  return reasonComponent;
};
