import React from "react";
import { useNavbarContext } from "../../context";

export const ToogleButton: React.FC = () => {
  const { state, dispatch } = useNavbarContext();

  const onClick = () => {
    dispatch({ key: "isOnline", payload: !state.isOnline });
  };

  return <button onClick={onClick}>Toogle Online</button>;
};
