import React from "react";

import styles from "./_styles.module.css";

export const NavbarLinks: React.FC = () => {
  return (
    <ul className={styles.links}>
      <li>Home</li>
      <li>About</li>
      <li>Contacts</li>
    </ul>
  );
};
