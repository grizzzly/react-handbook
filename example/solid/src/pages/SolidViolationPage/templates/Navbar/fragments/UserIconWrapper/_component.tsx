import React, { useMemo } from "react";
import { useMobileView } from "../../../../hooks";
import {
  eConnectStatus,
  eDisconnectReason,
  useNavbarContext,
} from "../../context";
import { DisconnectedCard } from "./DisconnectedCard";
import { UserIcon } from "./UserIcon";

export const UserIconWrapper: React.FC = () => {
  const {
    state: { anotherBrowserLogin, isOnline, connectStatus },
  } = useNavbarContext();

  const isMobile = useMobileView();

  const disconnectReason = useMemo<eDisconnectReason | null>(() => {
    if (!isOnline) return eDisconnectReason.NO_INTERNET_CONNECTION;

    if (connectStatus !== eConnectStatus.ONLINE)
      return eDisconnectReason.DISCONNECTED;

    if (anotherBrowserLogin) return eDisconnectReason.ANOTHER_BROWSER_LOGIN;

    return null;
  }, [anotherBrowserLogin, connectStatus, isOnline]);

  if (isMobile || disconnectReason === null) return <UserIcon />;

  return (
    <>
      <DisconnectedCard disconnectReason={disconnectReason}>
        <UserIcon />
      </DisconnectedCard>
    </>
  );
};
