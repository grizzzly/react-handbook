import React from "react";
import { IDisconnectedCard } from "./_types";
import { useDisconnectReasonComponent } from "./_useDisconnectedReasonComponent.hook";

import styles from "./_styles.module.css";

export const DisconnectedCard: React.FC<IDisconnectedCard> = ({
  children,
  disconnectReason,
}) => {
  const reasonComponent = useDisconnectReasonComponent(disconnectReason);

  return (
    <div className={styles.card}>
      <div className={styles.wrapper}>
        <div className={styles.row}>
          <p> {reasonComponent}</p>
          {children}
        </div>
        <p>
          Lorem ipsum dolor sit amet consectetur adipisicing elit. Beatae
          recusandae minima mollitia magnam necessitatibus iure...
        </p>
      </div>
    </div>
  );
};
