import { useEffect, useState } from "react";

export const useMobileView = (mobileWidth = 960) => {
  const [isMobileView, setIsMobileView] = useState(
    window.innerWidth <= mobileWidth
  );

  useEffect(() => {
    function resize() {
      if (window.innerWidth < mobileWidth) {
        setIsMobileView(true);
      } else {
        setIsMobileView(false);
      }
    }

    window.addEventListener("resize", resize);
    return () => window.removeEventListener("resize", resize);
  }, []);

  return isMobileView;
};
