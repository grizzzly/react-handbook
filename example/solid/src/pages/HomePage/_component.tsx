import React from "react";
import { UserTable } from "./templates";

export const HomePage: React.FC = () => {
  return (
    <>
      <UserTable />
    </>
  );
};
