import React from "react";

import { MappedUsers, ColumnTitles } from "./fragments";
import { useUserDataKeysAndTitlesToDisplay, useUsersData } from "./hooks";

import styles from "./_styles.module.css";

export const UserTable: React.FC = () => {
  const users = useUsersData();

  const { userKeys, columnTitles } = useUserDataKeysAndTitlesToDisplay();

  return (
    <table className={styles.table}>
      <tbody>
        <ColumnTitles titles={columnTitles} />
        <MappedUsers users={users} keys={userKeys} />
      </tbody>
    </table>
  );
};
