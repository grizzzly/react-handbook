import { IUser } from "../../_types";

import styles from "./_styles.module.css";

export const User: React.FC<{
  userData: IUser;
  keys: (keyof IUser)[];
}> = ({ userData, keys }) => {
  return (
    <tr className={styles.table_row}>
      {keys.map((key) => (
        <td key={key}>{userData[key]}</td>
      ))}
    </tr>
  );
};
