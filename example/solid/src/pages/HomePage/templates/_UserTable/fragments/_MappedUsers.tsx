import { IUser } from "../_types";
import { User } from "./User";

export const MappedUsers: React.FC<{
  users: IUser[];
  keys: (keyof IUser)[];
}> = ({ users, keys }) => (
  <>
    {users.map((userData) => (
      <User userData={userData} keys={keys} />
    ))}
  </>
);
