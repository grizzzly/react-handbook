import React from "react";

export const ColumnTitles: React.FC<{ titles: string[] }> = ({ titles }) => {
  return (
    <tr>
      {titles.map((title) => (
        <th>{title}</th>
      ))}
    </tr>
  );
};
