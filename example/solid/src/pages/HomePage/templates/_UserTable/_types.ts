export interface IUser {
  email: string;
  id: number;
  name: string;
  phone: string;
  username: string;
  website: string;
}

export type IUsersTableColumnsToDisplay = { key: keyof IUser; title: string }[];
