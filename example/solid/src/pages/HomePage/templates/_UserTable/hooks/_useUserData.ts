import { useEffect, useState } from "react";

import { IUser } from "../_types";

export const useUsersData = (): IUser[] => {
  const [users, setUsers] = useState<IUser[]>([]);

  useEffect(() => {
    fetch("https://jsonplaceholder.typicode.com/users/")
      .then((response) => response.json())
      .then((res) => setUsers(res));
  }, []);

  return users;
};
