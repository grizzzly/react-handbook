import { useMemo } from "react";

import { IUsersTableColumnsToDisplay } from "../_types";

export const useUserDataKeysAndTitlesToDisplay = () => {
  const userDataKeysAndTitlesToDisplay =
    useMemo<IUsersTableColumnsToDisplay>(() => {
      return [
        { key: "id", title: "id" },
        { key: "name", title: "Имя" },
        { key: "email", title: "Почта" },
        { key: "phone", title: "Телефон" },
        { key: "username", title: "Логин" },
        { key: "website", title: "сайт" },
      ];
    }, []);

  const userKeys = useMemo(
    () => userDataKeysAndTitlesToDisplay.map(({ key }) => key),
    [userDataKeysAndTitlesToDisplay]
  );

  const columnTitles = useMemo(
    () => userDataKeysAndTitlesToDisplay.map(({ title }) => title),
    [userDataKeysAndTitlesToDisplay]
  );

  return {
    userKeys,
    columnTitles,
    userDataKeysAndTitlesToDisplay,
  };
};
