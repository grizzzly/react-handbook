import React, { useContext, useReducer } from "react";

import { reducer } from "./_reducer";
import { INavbarContextProps } from "./_types";

export const NavbarContext = React.createContext({} as INavbarContextProps);

export const NavbarContextProvider: React.FC<{ children: React.ReactNode }> = ({
  children,
}) => {
  const [state, dispatch] = useReducer(reducer, {
    showSignInForm: false,
    showRegistrationForm: false,
  });

  return (
    <NavbarContext.Provider value={{ state, dispatch }}>
      {children}
    </NavbarContext.Provider>
  );
};

export const useNavbarContext = () => useContext(NavbarContext);
