import React from "react";
import {
  eNavbarActionType,
  INavbarAction,
  INavbarContextState,
} from "./_types";

export const reducer: React.Reducer<INavbarContextState, INavbarAction> = (
  state: INavbarContextState,
  { type, payload }: { type: eNavbarActionType; payload: any }
) => {
  switch (type) {
    case eNavbarActionType.SET_SHOW_SIGN_IN_FORM:
      return {
        ...state,
        showSignInForm: payload,
      };
    case eNavbarActionType.SET_SHOW_REGISTRATION_FORM:
      return {
        ...state,
        showRegistrationForm: payload,
      };
  }
};
