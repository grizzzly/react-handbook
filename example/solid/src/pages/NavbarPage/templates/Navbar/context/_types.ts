import React from "react";

export enum eNavbarActionType {
  SET_SHOW_SIGN_IN_FORM = "set show sign in form",
  SET_SHOW_REGISTRATION_FORM = "set show registration form",
}

export interface INavbarContextState {
  showSignInForm: boolean;
  showRegistrationForm: boolean;
}

export interface INavbarAction {
  type: eNavbarActionType;
  payload: any;
}

export interface INavbarContextProps {
  state: INavbarContextState;
  dispatch: React.Dispatch<INavbarAction>;
}
