import { eNavbarActionType, INavbarAction } from "./_types";

const setShowSignInForm = (show: boolean): INavbarAction => ({
  type: eNavbarActionType.SET_SHOW_SIGN_IN_FORM,
  payload: show,
});

const setShowRegistrationForm = (show: boolean): INavbarAction => ({
  type: eNavbarActionType.SET_SHOW_REGISTRATION_FORM,
  payload: show,
});

export const navbarActions = {
  setShowRegistrationForm,
  setShowSignInForm,
};
