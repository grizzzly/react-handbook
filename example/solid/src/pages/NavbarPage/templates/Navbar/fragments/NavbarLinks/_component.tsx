import React from "react";
import { links } from "./_constants";
import styles from "./_styles.module.css";

export const NavbarLinks: React.FC = () => {
  return (
    <ul className={styles.links}>
      {links.map((link) => (
        <a className={styles.link} href={link}>
          <li>{link}</li>
        </a>
      ))}
    </ul>
  );
};
