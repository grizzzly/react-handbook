import React from "react";

import { NavbarContextProvider } from "./context";
import { NavbarLogo, NavbarLinks } from "./fragments";

import styles from "./_styles.module.css";

export const Navbar: React.FC = () => {
  return (
    <NavbarContextProvider>
      <nav className={styles.navbar}>
        <NavbarLogo />
        <NavbarLinks />
      </nav>
    </NavbarContextProvider>
  );
};
