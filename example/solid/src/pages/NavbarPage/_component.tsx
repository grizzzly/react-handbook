import React from "react";
import { Navbar } from "./templates";

export const NavbarPage: React.FC = () => {
  return (
    <div>
      <Navbar />
    </div>
  );
};
